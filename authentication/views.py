from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate
from django.urls import reverse
from django.contrib.auth import login as django_login

from .forms import LoginForm

def login(request):
  if request.method == 'GET':
    form = LoginForm()
    return render(request, 'authentication/login.html', {'form': form})
  else:
    form = LoginForm(request.POST)
    if form.is_valid():
      cleaned_data = form.cleaned_data
      user = authenticate(username=cleaned_data['name'], password=cleaned_data['password'])
      if user is not None:
        django_login(request, user)
        return HttpResponseRedirect(reverse('lab_9:index'))
      else:
        return HttpResponseRedirect(reverse('lab_9:index'))

def logout(request):
  request.session.flush()
  return HttpResponseRedirect(reverse('lab_9:index'))