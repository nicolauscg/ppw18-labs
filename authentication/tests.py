from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib import auth

class AuthTest(TestCase):
  def test_login_url_exit(self):
    response = Client().get('/authentication/login')
    self.assertEqual(response.status_code,200)
  
  def test_logging_in(self):
    u = User(username='test-username')
    u.set_password('12345678')
    u.save()
    c = Client()
    c.post('/authentication/login', {'name': 'test-username', 'password': '12345678'})
    user = auth.get_user(c)
    self.assertTrue(user.is_authenticated)
  
  def test_logging_in_with_wrong_credentials(self):
    c = Client()
    c.post('/authentication/login', {'name': 'undefined-user', 'password': '12345678'})
    user = auth.get_user(c)
    self.assertFalse(user.is_authenticated)
  
  def test_logout(self):
    u = User(username='test-username')
    u.set_password('12345678')
    u.save()
    c = Client()
    c.post('/authentication/login', {'name': 'test-username', 'password': '12345678'})
    user = auth.get_user(c)
    self.assertTrue(user.is_authenticated)
    c.get('/authentication/logout')
    user = auth.get_user(c)
    self.assertFalse(user.is_authenticated)
