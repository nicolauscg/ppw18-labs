from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages

from .models import Activity
from .forms import ActivityForm

def index(request):
    return render(request, 'lab_4/index.html')

def detail(request):
    return render(request, 'lab_4/detail.html')
    
def register(request):
    if request.method == 'GET':
        return render(request, 'lab_4/register.html')
    else:
        messages.success(request, "Your form has been submitted! Well not really..")
        return HttpResponseRedirect('register')

def schedule(request):
    if request.method == 'GET':
        show_activity_count = 10
        form = ActivityForm()
        if Activity.objects.all().count() >= show_activity_count:
            activities = (Activity.objects.order_by('-date_time')[:show_activity_count])[::-1]
        else:
            activities = (Activity.objects.order_by('-date_time'))[::-1]
        
        return render(request, 'lab_4/schedule.html', { 'activities': activities, 'form': form })
    else:
        form = ActivityForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            new_activity = Activity(name=cleaned_data['name'], location=cleaned_data['location'], category=cleaned_data['category'], date_time=cleaned_data['date_time'])
            new_activity.save()
            return HttpResponseRedirect('schedule')
        else:
            messages.error(request, 'your form submission invalid :(')
            return HttpResponseRedirect('schedule')

def schedule_delete_all(request):
    if request.method == 'POST':
        info = Activity.objects.all().delete()
        messages.success(request, 'successfully deleted {:d} activity(s) :)'.format(info[0]))
        return HttpResponseRedirect('../schedule')
    else:
        messages.error(request, 'sorry the previous page is POST only')
        return HttpResponseRedirect('../schedule')
