from django import forms

class ActivityForm(forms.Form):
    name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'id': 'activity-name'}))
    location = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'id': 'activity-location'}))
    category = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'id': 'activity-category'}))
    date_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'id': 'activity-date_time'}))