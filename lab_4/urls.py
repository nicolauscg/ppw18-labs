from django.urls import path

from . import views

app_name = 'lab_4'
urlpatterns = [
    path('', views.index, name='index'),
    path('detail', views.detail, name='detail'),
    path('register', views.register, name='register'),
    path('schedule', views.schedule, name='schedule'),
    path('schedule/delete_all', views.schedule_delete_all, name='schedule_delete_all')
]