from django.test import TestCase, Client
from django.urls import resolve

from .views import index, register
from .forms import ActivityForm
from .models import Activity

from django.contrib.auth.models import User
from django.test import Client

class Lab4ViewTest(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/lab_4/')
        self.assertEqual(response.status_code,200)

    def test_index_using_index_template(self):
        response = Client().get('/lab_4/')
        self.assertTemplateUsed(response, 'lab_4/index.html')

    def test_index_using_index_func(self):
        found = resolve('/lab_4/')
        self.assertEqual(found.func, index)

    def test_detail_url_is_exist(self):
        response = Client().get('/lab_4/detail')
        self.assertEqual(response.status_code,200)

    def test_register_url_is_exist(self):
        response = Client().get('/lab_4/register')
        self.assertEqual(response.status_code,200)

    def test_redirect_to_register_after_form_submit(self):
        response = Client().post('/lab_4/register')
        self.assertEqual(response.url, 'register')

    def test_schdule_url_is_exist(self):
        response = Client().get('/lab_4/schedule')
        self.assertEqual(response.status_code,200)
    
    def test_schdule_form_is_valid(self):
        form_data = {
            'name': 'sample activity',
            'location': 'lab 1105',
            'category': 'ppw story',
            'date_time': '2018-10-12 09:14'
        
        }
        form = ActivityForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_schdule_activity_displayed_after_submitting_form(self):
        form_data = {
            'name': 'sample activity',
            'location': 'lab 1105',
            'category': 'ppw story',
            'date_time': '2018-10-12 09:14'
        }

        Client().post('/lab_4/schedule', form_data)
        get_response = Client().get('/lab_4/schedule')
        
        self.assertContains(get_response, 'sample activity')
    
    def test_schedule_delete_all(self):
        activities_count = 30
        form_data = {
            'name': 'sample activity',
            'location': 'lab 1105',
            'category': 'ppw story',
            'date_time': '2018-10-12 09:14'
        }

        for i in range(activities_count):
            Client().post('/lab_4/schedule', form_data)
        Client().post('/lab_4/schedule/delete_all')
        self.assertEqual(Activity.objects.all().count(), 0)

class Lab4ModelTest(TestCase):
    def model_have_string_representation(self):
        activity = Activity(name='name', location='location', category='category')
        self.assertEqual(activity.__str__(), 'name')