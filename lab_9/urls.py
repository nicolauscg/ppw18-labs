from django.urls import path, include, re_path
from django.contrib.auth import views

from . import views as lab9_view

app_name = 'lab_9'
urlpatterns = [
    path('', lab9_view.index, name='index'),
    path('register', lab9_view.register, name='register'),
    path('subscribers', lab9_view.subscribers, name='subscribers'),
    path('api/validate_email', lab9_view.api_validate_email, name='api_validate_email'),
    path('api/register', lab9_view.api_register, name='api_register'),
    path('api/update_counter', lab9_view.update_counter, name='update_counter'),
]