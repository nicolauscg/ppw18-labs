from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core import serializers

from .forms import SubscribeForm
from .models import Subscriber

def index(request):
  return render(request, 'lab_9/index.html')

def register(request):
  if request.method == 'GET':
    form = SubscribeForm()

    return render(request, 'lab_9/register.html', {'form': form})
  else:
    form = SubscribeForm(request.POST)
    if form.is_valid():
      cleaned_data = form.cleaned_data
      new_subscriber = Subscriber(name=cleaned_data['name'], email=cleaned_data['email'], password=cleaned_data['password'])
      new_subscriber.save()
    else:
      return HttpResponse('invalid')
    return HttpResponseRedirect(reverse('lab_9:register'))

def subscribers(request):
  data = Subscriber.objects.values_list('name', 'email')
  return render(request, 'lab_9/subscribers.html', {'data': data})

def api_validate_email(request):
  email = request.GET.get('q', None)
  data = [ obj.get_email() for obj in Subscriber.objects.all() ]
  if email == None or email in data:
    return JsonResponse({'valid': False})
  else:
    return JsonResponse({'valid': True})

def api_register(request):
  if (request.method == 'POST'):
    form = SubscribeForm(request.POST)
    if (form.is_valid()):
      name = form.cleaned_data['name']
      email = form.cleaned_data['email']
      password = form.cleaned_data['password']
      new_sub = Subscriber(name=name, email=email, password=password)
      new_sub.save()
      return JsonResponse({'success': True})
    return JsonResponse({'success': False})

def update_counter(request):
  if request.user.is_authenticated:
    if request.session.get('favourite_counter', None) == None:
      request.session['favourite_counter'] = 1
    else:
      if request.GET.get('q', None) == 'increment':
        request.session['favourite_counter'] += 1
      elif request.GET.get('q', None) == 'decrement':
        request.session['favourite_counter'] -= 1
    return JsonResponse({'success': True, 'counter': request.session.get('favourite_counter', None), 'q': request.POST.get('q')})
  return JsonResponse({'success': False})