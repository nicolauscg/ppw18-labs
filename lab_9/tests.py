from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
from django.contrib import auth
import json

from .views import index
from .models import Subscriber

class Lab9IndexTest(TestCase):
  def test_index_url_is_exist(self):
    response = Client().get('/lab_9/')
    self.assertEqual(response.status_code,200)

  def test_index_using_index_template(self):
    response = Client().get('/lab_9/')
    self.assertTemplateUsed(response, 'lab_9/index.html')

  def test_index_using_index_func(self):
    found = resolve('/lab_9/')
    self.assertEqual(found.func, index)

class Lab9SubscriberTest(TestCase):
  def test_register_url_exist(self):
    response = Client().get('/lab_9/register')
    self.assertEqual(response.status_code,200)

  def test_register_as_subscriber(self):
    response = Client().post('/lab_9/register', {'name': 'name', 'email': 'example@email.com', 'password': '12345678'})
    self.assertEqual(Subscriber.objects.all().count(), 1)

  def test_subscriber_url_exist(self):
    response = Client().get('/lab_9/subscribers')
    self.assertEqual(response.status_code,200)
  
  def test_api_check_unique_email(self):
    email = "new@email.com"
    response = Client().get('/lab_9/api/validate_email?q=' + email)
    json_response = json.loads(response.content)
    self.assertEqual(json_response['valid'], True)

  def test_api_check_not_unique_email(self):
    email = "new@email.com"
    c = Client()
    c.post('/lab_9/register', {'name': 'name', 'email': email, 'password': '12345678'})
    response = c.get('/lab_9/api/validate_email?q=' + email)
    json_response = json.loads(response.content)
    self.assertEqual(json_response['valid'], False)
  
  def test_api_register(self):
    c = Client()
    response = c.post('/lab_9/api/register', {'name': 'test', 'email': "example@email.com", 'password': '12345678'})
    json_response = json.loads(response.content)
    self.assertEqual(json_response['success'], True)
    self.assertEqual(Subscriber.objects.all().count(), 1)

  def test_update_counter_if_not_logged_in(self):
    c = Client()
    response = c.get('/lab_9/api/update_counter')
    json_response = json.loads(response.content)
    self.assertEqual(json_response['success'], False)

  def test_increment_counter(self):
    u = User(username='test-username')
    u.set_password('12345678')
    u.save()
    c = Client()
    c.post('/authentication/login', {'name': 'test-username', 'password': '12345678'})
    user = auth.get_user(c)
    self.assertTrue(user.is_authenticated)
    response = c.get('/lab_9/api/update_counter?q=increment')
    json_response = json.loads(response.content)
    self.assertEqual(json_response['counter'], 1)
  
  def test_decrement_counter(self):
    u = User(username='test-username')
    u.set_password('12345678')
    u.save()
    c = Client()
    c.post('/authentication/login', {'name': 'test-username', 'password': '12345678'})
    user = auth.get_user(c)
    self.assertTrue(user.is_authenticated)
    response = c.get('/lab_9/api/update_counter?q=increment')
    json_response = json.loads(response.content)
    self.assertEqual(json_response['counter'], 1)
    response = c.get('/lab_9/api/update_counter?q=decrement')
    json_response = json.loads(response.content)
    self.assertEqual(json_response['counter'], 0)


from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

class Lab6FunctionalTest(StaticLiveServerTestCase):
  def setUp(self):
    self.options = Options()
    self.options.add_argument('--dns-prefetch-disable')
    self.options.add_argument('--no-sandbox')
    self.options.add_argument('--headless')
    self.options.add_argument('disable-gpu')
    self.selenium = webdriver.Chrome(chrome_options=self.options, executable_path='./chromedriver')
    super(Lab6FunctionalTest, self).setUp()
  def tearDown(self):
    self.selenium.quit()
    super(Lab6FunctionalTest, self).tearDown()
  def test_counter_initially_zero(self):
    selenium = self.selenium
    selenium.get(self.live_server_url + '/lab_9/')
    time.sleep(2)
    self.assertTrue("0 " in selenium.find_element_by_id("counter").text)
  def test_counter_increment(self):
    selenium = self.selenium
    selenium.get(self.live_server_url + '/lab_9/')
    time.sleep(2)
    selenium.find_element_by_css_selector(".table .favourite-icon").click()
    self.assertTrue("1 " in selenium.find_element_by_id("counter").text)
  def test_counter_decrement(self):
    selenium = self.selenium
    selenium.get(self.live_server_url + '/lab_9/')
    time.sleep(2)
    selenium.find_element_by_css_selector(".table .favourite-icon").click()
    self.assertTrue("1 " in selenium.find_element_by_id("counter").text)
    time.sleep(1)
    selenium.find_element_by_css_selector(".table .favourite-icon").click()
    self.assertTrue("0 " in selenium.find_element_by_id("counter").text)

