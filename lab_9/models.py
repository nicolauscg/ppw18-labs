from django.db import models

class Subscriber(models.Model):
  name = models.CharField(max_length=40)
  email = models.EmailField(max_length=40, unique=True)
  password = models.CharField(max_length=40)

  def get_email(self):
    return self.email

  def __str__(self):
    return self.email