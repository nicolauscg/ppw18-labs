from django import forms

class SubscribeForm(forms.Form):
  name = forms.CharField(max_length=40, widget=forms.TextInput(attrs={
    'class': 'form-control',
  }))
  email = forms.EmailField(widget=forms.EmailInput(attrs={
    'class': 'form-control',
  }))
  password = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={
    'class': 'form-control',
  }))