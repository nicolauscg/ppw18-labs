$(document).ready(function () {
  // loading screen
  function showPage() {
    $('.loader').css("display", "none");
    $('.container').css("display", "block");
  }
  function f() {
    var a = setTimeout(showPage, 1000);
  }
  f();

  // remembers current theme
  if (localStorage.hasOwnProperty('theme')) {
    if(localStorage.getItem('theme') == 1){
      $("body").removeClass("change-theme");
    } else {
      $("body").addClass("change-theme");
    }
  } else {
    localStorage.setItem('theme', 1)
  }

  // change theme
  $('#change-theme-button').click(function () {
    if(localStorage.getItem('theme') == 1){
      $("body").addClass("change-theme");
      localStorage.setItem('theme', 2);
    } else {
      $("body").removeClass("change-theme");
      localStorage.setItem('theme', 1);
    }
  });

  // accordion
  $(".accordion__head").each(function(index) {
    $(this).click(function () {
      $(this).parent().toggleClass("expanded");
    });
  });
});