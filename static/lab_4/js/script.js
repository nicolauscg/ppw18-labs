$(document).ready(function () {
  $(".button-collapse").sideNav();
  $('.scrollspy').scrollSpy({scrollOffset: 64});
  $('.navbar').pushpin({
    top: $('.custom-wrapper').offset().top
  });
  
  $('.modal').modal();
  $('#modal1').modal('open');

  // datepicker options
  $('.datepicker').pickadate({
    format: 'yyyy-mm-dd',
    selectMonths: true,
    selectYears: 120,
    min: new Date(1900,0,1),
    max: new Date(2018,11,31),
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false
  });

});

// if window resized pushpin offset recalculated
$(document).scroll(function () {
  $('.navbar').pushpin({
    top: $('.custom-wrapper').offset().top
  });
});

// check if password and confirm password match
$("#password").on("focusout", function (e) {
  if ($(this).val() != $("#passwordConfirm").val()) {
      $("#passwordConfirm").removeClass("valid").addClass("invalid");
  } else {
      $("#passwordConfirm").removeClass("invalid").addClass("valid");
  }
});
$("#passwordConfirm").on("keyup", function (e) {
  if ($("#password").val() != $(this).val()) {
      $(this).removeClass("valid").addClass("invalid");
  } else {
      $(this).removeClass("invalid").addClass("valid");
  }
});

// check if datepicker input is empty
function checkDate() {
	if ($('#birthdate').val() == '') {
    $('#birthdate').addClass('invalid')
    return false;
  } else {
    $('#birthdate').removeClass('invalid')
    return true;
  }
}
$('#register-form').submit(function() {
  return checkDate();
});
$('#birthdate').change(function() {
  checkDate();
});

// check datetime in activity form using regex
function checkDateTime() {
  // does not check leap years, valid day is 01-31 on every month
  var regex = /^([0-2][0-9][0-9][0-9])-(0?[1-9]|1[0-2])-(0?[1-9]|1[0-9]|2[0-9]|3[0-1]) (0?[0-9]|1[0-9]|2[0-3]):([0-5][0-9])$/
  return regex.test($('#activity-date_time').val())
}
$('#activity-form').submit(function() { 
  return checkDateTime()
})
$('#activity-date_time').change(function() { 
  if (checkDateTime() == false) {
    $('#activity-date_time').addClass('invalid')
    return false;
  } else {
    $('#activity-date_time').removeClass('invalid')
    return true;
  }
})