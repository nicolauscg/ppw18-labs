from django.contrib import admin
from django.urls import path, include, re_path
from lab_9.views import index

urlpatterns = [
    path('', index, name='index'),
    path('lab_4/', include('lab_4.urls')),
    path('lab_6/', include('lab_6.urls')),
    path('lab_9/', include('lab_9.urls')),
    path('authentication/', include('authentication.urls')),
    path('admin/', admin.site.urls),
    re_path(r'^auth/', include('social_django.urls', namespace='social')),
]

# urlpatterns += [
#     path('accounts/', include('django.contrib.auth.urls')),
# ]