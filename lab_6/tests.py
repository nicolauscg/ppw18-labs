from django.test import TestCase, Client
from django.urls import resolve

from .views import index, profile
from .forms import MessageForm
from .models import Message

from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Lab6IndexTest(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/lab_6/')
        self.assertEqual(response.status_code,200)

    def test_index_using_index_template(self):
        response = Client().get('/lab_6/')
        self.assertTemplateUsed(response, 'lab_6/index.html')

    def test_index_using_index_func(self):
        found = resolve('/lab_6/')
        self.assertEqual(found.func, index)

    def test_index_form_is_valid_for_short_message(self):
        form_data = {'message': 'test message'}
        form = MessageForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_index_message_displayed_after_submitting_form(self):
        Client().post('/lab_6/', {'message': 'sample status'})
        get_response = Client().get('/lab_6/')
        self.assertContains(get_response, 'sample status')

    def test_index_message_deleted(self):
        Client().post('/lab_6/', {'message': 'sample status'})
        Client().post('/lab_6/message/1/delete')
        response = Client().get('/lab_6/')
        self.assertNotContains(response, 'sample status')

    def test_index_message_have_message_as_string_representaion(self):
        message_content = 'sample status'
        message = Message(message=message_content)

        self.assertEqual(message.__str__(), message_content)

class Lab6ProfileTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/lab_6/profile')
        self.assertEqual(response.status_code,200)

    def test_index_using_index_template(self):
        response = Client().get('/lab_6/profile')
        self.assertTemplateUsed(response, 'lab_6/profile.html')

    def test_index_using_index_func(self):
        found = resolve('/lab_6/profile')
        self.assertEqual(found.func, profile)

class Lab6FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        self.options = Options()
        self.options.add_argument('--dns-prefetch-disable')
        self.options.add_argument('--no-sandbox')        
        self.options.add_argument('--headless')
        self.options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=self.options, executable_path='./chromedriver')
        super(Lab6FunctionalTest, self).setUp()
    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()
    def test_title(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/lab_6/')
        time.sleep(2)
        self.assertEqual(selenium.title, 'Nicolaus')
    def test_add_status(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/lab_6/')
        time.sleep(2)
        selenium.find_element_by_id("update-button").click()
        time.sleep(2)
        selenium.find_element_by_tag_name("textarea").send_keys("this is a test message")
        selenium.find_element_by_id("submit-button").click()
        time.sleep(2)
        self.assertIn("this is a test message", selenium.page_source)
    def test_delete_status(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/lab_6/')
        time.sleep(2)
        selenium.find_element_by_id("update-button").click()
        time.sleep(2)
        selenium.find_element_by_tag_name("textarea").send_keys("this is a test message")
        selenium.find_element_by_id("submit-button").click()
        time.sleep(2)
        selenium.find_element_by_id("delete-status").click()
        time.sleep(2)
        self.assertNotIn("this is a test message", selenium.page_source)
        selenium = self.selenium
        selenium.get(self.live_server_url + '/lab_6/')
        time.sleep(2)
        selenium.find_element_by_id("update-button").click()
        time.sleep(2)
        selenium.find_element_by_tag_name("textarea").send_keys("this is a test message")
        selenium.find_element_by_id("submit-button").click()
        time.sleep(2)
        self.assertIn("this is a test message", selenium.page_source)
    def test_delete_status(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/lab_6/')
        time.sleep(2)
        selenium.find_element_by_id("update-button").click()
        time.sleep(2)
        selenium.find_element_by_tag_name("textarea").send_keys("this is a test message")
        selenium.find_element_by_id("submit-button").click()
        time.sleep(2)
        selenium.find_element_by_id("delete-status").click()
        time.sleep(2)
        self.assertNotIn("this is a test message", selenium.page_source)
    def test_page_have_header(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/lab_6/')
        time.sleep(2)
        header = selenium.find_elements_by_tag_name("h1")
        self.assertEqual(len(header), 1)
        self.assertEqual(header[0].text, 'Hello, how are you?')
    def test_page_have_see_profile_button(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/lab_6/')
        time.sleep(2)
        buttons = selenium.find_elements_by_css_selector('a.btn')
        found = False
        for button in buttons:
            if (button.text == 'See profile'): 
                found = True
        self.assertTrue(found)
    def test_header_font_size(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/lab_6/')
        time.sleep(2)
        ele = selenium.find_element_by_css_selector('h1')
        self.assertEqual(ele.value_of_css_property('font-size'), "96px")
    def test_button_background_color(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/lab_6/')
        time.sleep(2)
        ele = selenium.find_element_by_class_name('btn')
        self.assertEqual(ele.value_of_css_property('background-color'), "rgba(52, 58, 64, 1)")