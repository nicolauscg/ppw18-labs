from django import forms

class MessageForm(forms.Form):
    message = forms.CharField(max_length=100, widget=forms.Textarea(attrs={
        'id': 'message-form-message',
        'class': 'form-control',
        'placeholder':'message',
        'rows': '3',
    }))