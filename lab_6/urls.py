from django.urls import path

from . import views

app_name = 'lab_6'
urlpatterns = [
    path('', views.index, name='index'),
    path('profile', views.profile, name='profile'),
    path('message/<int:message_id>/delete', views.delete, name='delete'),
]