from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect

from .models import Message
from .forms import MessageForm

def index(request):
    if request.method == 'GET':
        form = MessageForm()
        messages = Message.objects.all()

        return render(request, 'lab_6/index.html', {'messages': messages, 'form': form})
    else:
        form = MessageForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            new_message = Message(message=cleaned_data['message'])
            new_message.save()

            return HttpResponseRedirect('./')

def delete(request, message_id):
    message = get_object_or_404(Message, pk=message_id)
    message.delete()

    return HttpResponseRedirect('../../')

def profile(request):
    return render(request, 'lab_6/profile.html')